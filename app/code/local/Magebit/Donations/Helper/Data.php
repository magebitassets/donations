<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Is the module enabled?
     *
     * @return bool
     */
    public function isEnabled()
    {
        return boolval(Mage::getStoreConfig('magebit_donations/general/enabled'));
    }

    /**
     * Returns info text
     *
     * @return float
     */
    public function getInfoText()
    {
        return Mage::getStoreConfig('magebit_donations/general/info');
    }

    /**
     * Returns donate label
     *
     * @return float
     */
    public function getDonateLabel()
    {
        return Mage::getStoreConfig('magebit_donations/general/donate_label');
    }

    /**
     * Returns donate label
     *
     * @return float
     */
    public function getGiftAidLabel()
    {
        return Mage::getStoreConfig('magebit_donations/general/gift_aid_label');
    }

    /**
     * Returns donation amount for one product
     *
     * @return float
     */
    public function getDonationPrice()
    {
        return floatval(Mage::getStoreConfig('magebit_donations/general/amount'));
    }

    /**
     * Returns the donation amount for the given order
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @return float
     */
    public function getDonationAmountForOrder(Mage_Sales_Model_Order $order)
    {
        $amount = 0;

        foreach (Mage::getModel('magebit_donations/donation')->getByOrder($order) as $donation) {
            $amount += $donation->getAmount();
        }

        return $amount;
    }

    /**
     * Gets total amount of donation for current session
     *
     * @return float
     */
    public function getDonationAmount()
    {
        $qty = 0;

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $items = $quote->getAllVisibleItems();

        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            $buyRequest = unserialize($item->getOptionByCode('info_buyRequest')->getValue());

            if (isset($buyRequest['donate']) && $buyRequest['donate'] == 1) {
                $qty++;
            }
        }

        return $qty * $this->getDonationPrice();
    }
}