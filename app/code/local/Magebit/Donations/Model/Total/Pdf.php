<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Model_Total_Pdf extends Mage_Sales_Model_Order_Pdf_Total_Default
{

    /**
     * Gets the amount of donations
     *
     * @return float
     */
    public function getAmount()
    {
        return Mage::helper('magebit_donations')->getDonationAmountForOrder($this->getOrder());
    }
}