<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Model_Total_Quote extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    /**
     * Set code
     */
    public function __construct()
    {
        $this->setCode('magebit_donation');
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return Mage::helper('magebit_donations')->__('Donation');
    }

    /**
     * Collect totals information about insurance
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     *
     * @return $this|Mage_Sales_Model_Quote_Address_Total_Abstract
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        if (($address->getAddressType() == 'billing')) {
            return $this;
        }

        $amount = Mage::helper('magebit_donations')->getDonationAmount();

        if ($amount) {
            $this->_addAmount($amount);
            $this->_addBaseAmount($amount * Mage::app()->getStore()->getCurrentCurrencyRate());
        }

        return $this;
    }

    /**
     * Add totals information to address object
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     *
     * @return $this|array
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        if (($address->getAddressType() == 'billing')) {
            $amount = Mage::helper('magebit_donations')->getDonationAmount();
            if ($amount != 0) {
                $address->addTotal(array(
                    'code'  => $this->getCode(),
                    'title' => $this->getLabel(),
                    'value' => $amount
                ));
            }
        }

        return $this;
    }
}