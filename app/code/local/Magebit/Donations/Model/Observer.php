<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Model_Observer
{

    /**
     * Adds donations to paypal cart
     *
     * @param Varien_Event_Observer $observer
     *
     * @return void
     */
    public function paypalPrepareLineItems(Varien_Event_Observer $observer)
    {
        $paypal_cart = $observer->getPaypalCart();
        if ($paypal_cart && $paypal_cart->getSalesEntity()) {
            $amount = Mage::helper('magebit_donations')->getDonationAmount();

            if ($amount) {
                $paypal_cart->addItem(Mage::helper('magebit_donations')->__('Donation'), 1, $amount, 'donation');
            }
        }
    }

    /**
     * We create the donation entries when the order is saved into db
     *
     * @param Varien_Event_Observer $observer
     *
     * @return void
     */
    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        $quoteItem = $observer->getItem();

        /** @var Mage_Sales_Model_Order_item $orderItem */
        $orderItem = $observer->getOrderItem();

        if ($quoteItem->getParentItem()) {
            return;
        }

        $buyRequest = unserialize($quoteItem->getOptionByCode('info_buyRequest')->getValue());
        if (isset($buyRequest['donate']) && $buyRequest['donate'] == 1) {
            // donated, so we create the donation for this item

            $billingAddress = $quote->getBillingAddress();
            $fullAddress = sprintf('%s, %s, %s', implode(' ', $billingAddress->getStreet()),
                mb_strtoupper($billingAddress->getCity(), 'UTF-8'), $billingAddress->getPostcode());

            $createData = array(
                'amount'           => Mage::helper('magebit_donations')->getDonationPrice(),
                'product_sku'      => $orderItem->getSku(),
                'gift_aid'         => (isset($buyRequest['gift_aid']) && $buyRequest['gift_aid'] == 1) ? 1 : 0,
                'customer_name'    => $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname(),
                'customer_address' => $fullAddress,
                'created_at'       => Mage::getSingleton('core/date')->gmtDate(),
                'quote_id'         => $quote->getId()
            );

            // insert into db
            Mage::getModel('magebit_donations/donation')->addData($createData)->save();
        }
    }
}