<?php
/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('magebit_donations/donation');
$installer->run("DROP TABLE IF EXISTS {$tableName} ");

$table = $installer->getConnection()
    ->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'ID')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => false,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => false,
    ), 'Quote ID')
    ->addColumn('amount', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'unsigned' => true,
    ), 'Donation Amount')
    ->addColumn('gift_aid', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(), 'Gift Aid')
    ->addColumn('product_sku', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'length'   => 255,
    ), 'Product SKU')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false,
    ), 'Created At')
    ->addColumn('customer_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'length'   => 255,
    ), 'Customer Name')
    ->addColumn('customer_address', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'length'   => 255,
    ), 'Customer Address');

$installer->getConnection()->createTable($table);

$installer->endSetup();