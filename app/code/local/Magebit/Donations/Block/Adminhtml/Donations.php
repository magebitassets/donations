<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Block_Adminhtml_Donations extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'magebit_donations';
        $this->_controller = 'adminhtml_donations';
        $this->_headerText = Mage::helper('magebit_donations')->__('Donations');

        parent::__construct();

        $this->removeButton('add');
    }

}