<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Block_Adminhtml_Donations_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('collection_donations');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir(Varien_Data_Collection::SORT_ORDER_DESC);
        $this->setUseAjax(true);
        $this->setDefaultLimit(30);
    }

    /**
     * Creates collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getModel('magebit_donations/donation')->getCollection());

        return parent::_prepareCollection();
    }

    /**
     * Prepares columns to be rendered
     *
     * @return mixed
     */
    protected function _prepareColumns()
    {
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));

        $this->addColumn('created_at', array(
            'header' => $this->__('Date'),
            'type'   => 'datetime',
            'index'  => 'created_at'
        ));

        $this->addColumn('amount', array(
            'header' => $this->__('Donation Amount'),
            'type'   => 'number',
            'index'  => 'amount',
        ));

        $this->addColumn('gift_aid', array(
            'header'  => $this->__('Gift Aid'),
            'type'    => 'options',
            'options' => array(
                0 => 'No',
                1 => 'Yes'
            ),
            'index'   => 'gift_aid'
        ));

        $this->addColumn('product_sku', array(
            'header' => $this->__('Product SKU'),
            'type'   => 'text',
            'index'  => 'product_sku',
        ));

        $this->addColumn('customer_name', array(
            'header' => $this->__('Customer Name'),
            'type'   => 'text',
            'index'  => 'customer_name',
        ));

        $this->addColumn('customer_address', array(
            'header' => $this->__('Address'),
            'type'   => 'text',
            'index'  => 'customer_address',
        ));

        return parent::_prepareColumns();
    }

    /**
     * Grid AJAX URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * No url for row
     *
     * @return null
     */
    public function getRowUrl()
    {
        return null;
    }
}