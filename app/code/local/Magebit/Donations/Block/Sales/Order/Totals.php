<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals
{

    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
        parent::_initTotals();

        $amount = Mage::helper('magebit_donations')->getDonationAmountForOrder($this->getOrder());

        if ($amount) {
            $this->addTotalBefore(new Varien_Object(array(
                'code'       => 'magebit_donations',
                'value'      => $amount,
                'base_value' => $amount * $this->getOrder()->getStoreToBaseRate(),
                'label'      => $this->helper('magebit_donations')->__('Donations'),
            ), array('shipping', 'tax')));
        }

        return $this;
    }

}