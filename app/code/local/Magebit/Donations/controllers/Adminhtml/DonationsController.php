<?php

/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Donations_Adminhtml_DonationsController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Common layout DRY
     *
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('magebit_donations/manage')
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Donations'),
                Mage::helper('adminhtml')->__('Donations')
            );

        return $this;
    }

    /**
     * Shows all donations
     *
     * @return void
     */
    public function indexAction()
    {
        $this->_title($this->__('Donations'));
        $this->_title($this->__('Donations'));
        $this->_initAction();

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('magebit_donations/adminhtml_donations'));

        $this->renderLayout();
    }

    /**
     * Shows sticker ordered items in a grid
     *
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('magebit_donations/adminhtml_donations_grid')->toHtml()
        );
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'donations.csv';
        $grid = $this->getLayout()->createBlock('magebit_donations/adminhtml_donations_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
}