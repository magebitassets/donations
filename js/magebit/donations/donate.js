/**
 * Magebit_Donations
 *
 * @category     Magebit
 * @package      Magebit_Donations
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2016 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
jQuery(function ($) {
    $('.match-donation').change(function(){
        if( $(this).is(':checked') )
        {
            $('.match-donation-gift-aid').addClass( 'active' );
        }
        else
        {
            $('.match-donation-gift-aid').removeClass( 'active' );
        }
    });
});